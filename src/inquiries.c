/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inquiries.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 15:04:47 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/08/17 13:07:01 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

int				stack_top(t_stack *stack)
{
	return (stack->arr[stack->size -1].num);
}

int				stack_bottom(t_stack *stack)
{
	return (stack->arr[0].num);
}

size_t			get_score(stack_int n)
{
	size_t		score;
	int			up;
	int			down;

	score = 0;
	down = n.sort_down;
	up = n.sort_up;
	if (down == 0 && up == 0)
		return (abs(n.rot));
	if (n.rot == 0)
		return (up >= down ? down : up);
	if (down == up && n.rot > 0)
		return (n.rot >= up? n.rot: up);
	if (down == up && n.rot < 0)
	{
		if (abs(n.rot) >= down)
			return (abs(n.rot));
		return (down);
	}
	if (n.rot > 0 && n.rot >= up)
		return (n.rot);
	if (n.rot < 0 && abs(n.rot) >= down)
		return (abs(n.rot));
	if (down < up)
	{
		if (n.rot > 0 && n.rot < up && up - n.rot <= down)
			return (n.rot + (up - n.rot));
		if (n.rot > 0 && n.rot < up && up - n.rot >= down)
			return (abs(n.rot) + down);
		if (n.rot > 0 && n.rot < up && up - n.rot <= down)
			return (n.rot + (up - n.rot));
		if (n.rot < 0)
			return (abs(n.rot) >= down? abs(n.rot): abs(n.rot) + (down - abs(n.rot)));
	}
	if (up < down)
	{
		if (n.rot < 0 && abs(n.rot) < down && down - n.rot <= up)
			return (abs(n.rot) + (down - abs(n.rot)));
		if (n.rot < 0 && abs(n.rot) < down && down - abs(n.rot) >= up)
			return (abs(n.rot) + up);
		if (n.rot < 0 && abs(n.rot) < down && down - abs(n.rot) <= up)
			return (abs(n.rot) + (down - abs(n.rot)));
		if (n.rot > 0)
			return (abs(n.rot) >= up? abs(n.rot): abs(n.rot) + (up - abs(n.rot)));
	}
	return (score);
}