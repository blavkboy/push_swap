/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   analysis.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 09:57:22 by gladi8r           #+#    #+#             */
/*   Updated: 2018/08/20 06:28:17 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

void			even_update(t_stacks **stacks)
{
	size_t		i;

	i = 0;
	while (i < (*stacks)->stack_a->size / 2)
	{
		(*stacks)->stack_a->arr[i].rot = (i + 1) * -1;
		i++;
	}
	i = (*stacks)->stack_a->size;
	while (i > (*stacks)->stack_a->size / 2)
	{
		(*stacks)->stack_a->arr[i - 1].rot = ((*stacks)->stack_a->size - i);
		i--;
	}
	num_rotations(stacks);
}

void			even_update_b(t_stacks **stacks)
{
	size_t		i;

	i = 0;
	while (i < (*stacks)->stack_b->size / 2)
	{
		(*stacks)->stack_b->arr[i].rot = (i + 1) * -1;
		i++;
	}
	i = (*stacks)->stack_b->size;
	while (i > (*stacks)->stack_b->size / 2)
	{
		(*stacks)->stack_b->arr[i - 1].rot = ((*stacks)->stack_b->size - i);
		i--;
	}
}

void			odd_update(t_stacks **stacks)
{
	size_t		i;

	i = 0;
	while (i < (*stacks)->stack_a->size / 2)
	{
		(*stacks)->stack_a->arr[i].rot = (i + 1) * -1;
		i++;
	}
	(*stacks)->stack_a->arr[(*stacks)->stack_a->size / 2].rot =
		(*stacks)->stack_a->size / 2;
	i = (*stacks)->stack_a->size;
	while (i > (*stacks)->stack_a->size / 2)
	{
		(*stacks)->stack_a->arr[i - 1].rot = ((*stacks)->stack_a->size - i);
		i--;
	}
	num_rotations(stacks);
}

void			odd_update_b(t_stacks **stacks)
{
	size_t		i;

	i = 0;
	while (i < (*stacks)->stack_b->size / 2)
	{
		(*stacks)->stack_b->arr[i].rot = (i + 1) * -1;
		i++;
	}
	(*stacks)->stack_b->arr[(*stacks)->stack_b->size / 2].rot =
		(*stacks)->stack_b->size / 2;
	i = (*stacks)->stack_b->size;
	while (i > (*stacks)->stack_b->size / 2)
	{
		(*stacks)->stack_b->arr[i - 1].rot = ((*stacks)->stack_b->size - i);
		i--;
	}
}

void			update_stacks(t_stacks **stacks)
{
	if ((*stacks)->stack_a->size % 2 == 0)
		even_update(stacks);
	else
		odd_update(stacks);
	if ((*stacks)->stack_b->size % 2 == 0)
		even_update_b(stacks);
	else
		odd_update_b(stacks);
}