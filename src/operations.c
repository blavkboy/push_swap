/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   operations.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/11 14:23:04 by gladi8r           #+#    #+#             */
/*   Updated: 2018/08/12 12:00:18 by gladi8r          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

void				rot_stack(t_stacks **stacks, char *s)
{
	if (!ft_strcmp(s, "a"))
		ft_stack_rotate(&(*stacks)->stack_a);
	if (!ft_strcmp(s, "b"))
		ft_stack_rotate(&(*stacks)->stack_b);
	if (!ft_strcmp(s, "aa"))
		ft_stack_rev_rotate(&(*stacks)->stack_a);
	if (!ft_strcmp(s, "bb"))
		ft_stack_rev_rotate(&(*stacks)->stack_b);
	if (!ft_strcmp(s, "rr"))
	{
		ft_stack_rotate(&(*stacks)->stack_a);
		ft_stack_rotate(&(*stacks)->stack_b);
	}
	if (!ft_strcmp(s, "-r"))
	{
		ft_stack_rev_rotate(&(*stacks)->stack_a);
		ft_stack_rev_rotate(&(*stacks)->stack_b);
	}
}

void				swap_a(t_stacks **stacks)
{
	stack_int		temp1;
	stack_int		temp2;

	bzero(&temp1, sizeof(stack_int));
	bzero(&temp2, sizeof(stack_int));
	if (!(*stacks)->stack_a->size || (*stacks)->stack_a->size == 1)
		return ;
	temp1 = ft_stack_pop(&(*stacks)->stack_a);
	temp2 = ft_stack_pop(&(*stacks)->stack_a);
	ft_stack_push(&(*stacks)->stack_a, temp1.num);
	ft_stack_push(&(*stacks)->stack_a, temp2.num);
}

void				swap_b(t_stacks **stacks)
{
	stack_int		temp1;
	stack_int		temp2;

	bzero(&temp1, sizeof(stack_int));
	bzero(&temp2, sizeof(stack_int));
	if (!(*stacks)->stack_b->size || (*stacks)->stack_b->size == 1)
		return ;
	temp1 = ft_stack_pop(&(*stacks)->stack_b);
	temp2 = ft_stack_pop(&(*stacks)->stack_b);
	ft_stack_push(&(*stacks)->stack_b, temp1.num);
	ft_stack_push(&(*stacks)->stack_b, temp2.num);
}

void				swap_pop(t_stacks **stacks, char *str)
{
	if (!ft_strcmp(str, "a"))
		swap_a(stacks);
	if (!ft_strcmp(str, "b"))
		swap_b(stacks);
	if (!ft_strcmp(str, "s"))
	{
		swap_a(stacks);
		swap_b(stacks);
	}
}

void				push_stacks(t_stacks **stacks, char *str)
{
	stack_int		temp;

	bzero((void*)&temp, sizeof(stack_int));
	if (!ft_strcmp(str, "pa"))
		if ((*stacks)->stack_b->size)
		{
			temp = ft_stack_pop(&(*stacks)->stack_b);
			ft_stack_push(&(*stacks)->stack_a, temp.num);
		}
	if (!ft_strcmp(str, "pb"))
	{
		if ((*stacks)->stack_a->size)
		{
			temp = ft_stack_pop(&(*stacks)->stack_a);
			ft_stack_push(&((*stacks)->stack_b), temp.num);
		}
	}
}
