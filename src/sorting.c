/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sorting.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/11 19:39:52 by gladi8r           #+#    #+#             */
/*   Updated: 2018/08/24 09:23:23 by gladi8r          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

static stack_int		best_score(t_stacks **stacks)
{
	size_t				pos;
	size_t				smallest;
	int					i;

	smallest = 2147483647;
	pos = 0;
	i = (*stacks)->stack_a->size - 1;
	while (i >= 0)
	{
		if (get_score((*stacks)->stack_a->arr[i]) < smallest)
		{
			smallest = get_score((*stacks)->stack_a->arr[i]);
			pos = i;
		}
		i--;
	}
	return ((*stacks)->stack_a->arr[pos]);
}

void			rotate_stacks(t_stacks **stacks, int rot)
{
	if (rot > 0)
	{
		while (rot > 0)
		{

			operations(stacks, "rb");
			ft_putendl("rb");
			rot--;
		}
	}
	else
	{
		while (rot < 0)
		{
			operations(stacks, "rrb");
			ft_putendl("rrb");
			rot++;
		}
	}
}

void			sort_stacks(t_stacks **stacks)
{
	stack_int	biggest;
	int			rot;

	if ((*stacks)->stack_a->size > 5)
	{
		while ((*stacks)->stack_a->size)
		{
			update_stacks(stacks);
			best_move(best_score(stacks), stacks);
		}
		update_stacks(stacks);
		biggest = find_biggest(stacks);
		rot = biggest.rot;
		rotate_stacks(stacks, rot);
		while ((*stacks)->stack_b->size)
		{
			operations(stacks, "pa");
			ft_putendl("pa");
		}
	}
	else
		quick_sort(stacks);
	update_stacks(stacks);
}

int				confirm_set(t_stacks **stacks)
{
	size_t		i;
	size_t		y;
	t_stack		*stack;
	
	if ((*stacks)->stack_b->size <= 1)
		return (1);
	stack = (*stacks)->stack_b;
	i = 1;
	while (i < stack->size)
	{
		y = 0;
		while (y < i)
		{
			if (stack->arr[y].num == stack->arr[i].num)
				return (0);
			y++;
		}
		i++;
	}
	return (1);
}
