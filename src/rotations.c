/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotations.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/21 10:01:56 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/08/21 10:01:58 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

static stack_int	smallest_sort(t_stacks **stacks, size_t index)
{
	stack_int		n;
	int				min;
	size_t			i;
	size_t			j;

	i = (*stacks)->stack_b->size;
	min = 2147483647;
	j = 1;
	n = (*stacks)->stack_a->arr[index - 1]; 
	while (i)
	{
		if (min > (*stacks)->stack_b->arr[i - 1].num)
		{
			min = (*stacks)->stack_b->arr[i - 1].num;
			n.sort_up = (j == (*stacks)->stack_b->size? 0: j);
			n.sort_down = (n.sort_up == 0? 0: (*stacks)->stack_b->size - n.sort_up);
		}
		j++;
		i--;
	}
	return (n);
}

static void		__sort_stacks_up(t_stacks **stacks, size_t index)
{
	stack_int	n;
	int			max;
	size_t		i;
	size_t		j;

	n = (*stacks)->stack_a->arr[index - 1];
	max = -2147483647;
	i = (*stacks)->stack_b->size;
	j = 1;
	while (i)
	{
		if ((*stacks)->stack_b->arr[i - 1].num < n.num &&
			(*stacks)->stack_b->arr[i - 1].num > max)
		{
			max = (*stacks)->stack_b->arr[i - 1].num;
			n.sort_up = (i == (*stacks)->stack_b->size? 0: (*stacks)->stack_b->size - i);
			n.sort_down = (n.sort_up == 0? 0: (*stacks)->stack_b->size - n.sort_up);
		}
		j++;
		i--;
	}
	n = (max == -2147483647? smallest_sort(stacks, index): n);
	(*stacks)->stack_a->arr[index - 1] = n;
}

void			num_rotations(t_stacks **stacks)
{
	size_t		direction;

	direction = (*stacks)->stack_a->size;
	if ((*stacks)->stack_b->size <= 1)
		return ;
	while (direction > 0)
	{
		__sort_stacks_up(stacks, direction);
		direction--;
	}
}

stack_int		find_biggest(t_stacks **stacks)
{
	int			big;
	size_t		pos;
	size_t		i;

	big = -2147483647;
	pos = 0;
	i = 0;
	while (i < (*stacks)->stack_b->size)
	{
		if ((*stacks)->stack_b->arr[i].num > big)
		{
			big = (*stacks)->stack_b->arr[i].num;
			pos = i;
		}
		i++;
	}
	return ((*stacks)->stack_b->arr[pos]);
}