/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sorted.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/11 14:28:54 by gladi8r           #+#    #+#             */
/*   Updated: 2018/08/11 19:39:33 by gladi8r          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

void					confirm_sort(t_stacks *stacks)
{
	size_t				i;
	int				prev;

	if (!stacks->stack_b->size)
	{
		i = 1;
		prev = stacks->stack_a->arr[0].num;
		while (i < stacks->stack_a->size)
		{
			if (stacks->stack_a->arr[i].num > prev)
			{
				ft_putsred("KO\n");
				return ;
			}
			prev = stacks->stack_a->arr[i].num;
			i++;
		}
		ft_putsgreen("OK\n");
		return ;
	}
	ft_putsred("KO\n");
}
