/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/11 19:30:45 by gladi8r           #+#    #+#             */
/*   Updated: 2018/08/24 09:23:56 by gladi8r          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

static void				reverse(t_stacks **stacks)
{
	while ((*stacks)->stack_b->size > 0)
		operations(stacks, "pa");
}

static int				parse_args(char **av)
{
	size_t			i;
	
	i = 1;
	while(av[i])
	{
		if (check_args(av[i]))
			i++;
		else
		{
			ft_putendl("Error");
			return (0);
		}
	}
	return (1);
}

static void				get_data(t_stacks **stacks, char **av)
{
	size_t				i;

	i = 1;
	while (av[i])
	{
		make_stacks(stacks, av[i]);
		i++;
	}
	get_args(stacks, av);
}

int			main(int ac, char **av)
{
	t_stacks	*stacks;

	if (ac > 1)
	{
		stacks = init_stacks();
		if (!parse_args(av))
			return (1);
		get_data(&stacks, av);
		reverse(&stacks);
		if (!confirm_set(&stacks))
			return (1);
		update_stacks(&stacks);
		stacks->eval = 0;
		sort_stacks(&stacks);
		empty_stacks(&stacks);
	}
	return (0);
}
