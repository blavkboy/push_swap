#include <push_swap.h>

void			rot_only(stack_int n, t_stacks **stacks)
{
	if (n.rot == 0)
	{
		operations(stacks, "pb");
		ft_putendl("pb");
	}
	while (n.rot != 0)
	{
		if (n.rot > 0)
		{
			operations(stacks, "ra");
			ft_putendl("ra");
			n.rot--;
		}
		else
		{
			operations(stacks, "rra");
			ft_putendl("rra");
			n.rot++;
		}
	}
}

void			short_sides(int up, int down, t_stacks **stacks)
{
	if (up < down)
	{
		while (up)
		{
			operations(stacks, "rb");
			ft_putendl("rb");
			up--;
		}
	}
	else
	{
		while (down)
		{
			operations(stacks, "rrb");
			ft_putendl("rrb");
			down--;
		}
	}
}

void			side_up(int up, stack_int n, t_stacks **stacks)
{
	while (up != 0 || n.rot != 0)
	{
		if (up != 0 && n.rot != 0)
		{
			operations(stacks, "rr");
			ft_putendl("rr");
			n.rot--;
			up--;
		}
		else if (up != 0 && n.rot == 0)
		{
			operations(stacks, "rb");
			ft_putendl("rb");
			up--;
		}
		else if (n.rot != 0 && up == 0)
		{
			operations(stacks, "ra");
			ft_putendl("ra");
			n.rot--;
		}
	}
}

void			side_down(int down, stack_int n, t_stacks **stacks)
{
	while (n.rot != 0 || down != 0)
	{
		if (n.rot != 0 && down != 0)
		{
			operations(stacks, "rrr");
			ft_putendl("rrr");
			down--;
			n.rot++;
		}
		else if (n.rot != 0)
		{
			operations(stacks, "rra");
			ft_putendl("rra");
			n.rot++;
		}
		else if (down != 0)
		{
			operations(stacks, "rrb");
			ft_putendl("rrb");
			down--;
		}
	}
}

void			mid_up_down(int up, int down, stack_int n, t_stacks **stacks)
{
	if (n.rot > 0)
	{
		side_up(0, n, stacks);
		n.rot = 0;
		side_down(down, n, stacks);
	}
	else if (n.rot < 0)
	{
		side_down(0, n, stacks);
		n.rot = 0;
		side_up(up, n, stacks);
	}
}