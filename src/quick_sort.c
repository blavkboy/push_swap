
#include <push_swap.h>

static stack_int		find_smallest(t_stacks **stacks)
{
	size_t				pos;
	size_t				i;
	int					max;

	i = 0;
	max = 2147483647;
	pos = 0;
	while (i < (*stacks)->stack_a->size)
	{
		if ((*stacks)->stack_a->arr[i].num < max)
		{
			max = (*stacks)->stack_a->arr[i].num;
			pos = i;
		}
		i++;
	}
	return ((*stacks)->stack_a->arr[pos]);
}

static void				mk_3(t_stacks **stacks)
{
	stack_int			n;
	int					rot;

	update_stacks(stacks);
	n = find_smallest(stacks);
	rot = n.rot;
	while (rot != 0)
	{
		if (rot > 0)
		{
			operations(stacks, "ra");
			ft_putendl("ra");
			rot--;
		}
		else
		{
			operations(stacks, "rra");
			ft_putendl("rra");
			rot++;
		}
	}
	operations(stacks, "pb");
	ft_putendl("pb");
}

static void				top_flip(t_stacks **stacks)
{
	t_stack				*stack;

	stack = (*stacks)->stack_a;
	if (stack->size == 3)
	{
		if (stack->arr[1].num < stack->arr[2].num)
		{
			operations(stacks, "sa");
			ft_putendl("sa");
		}
	}
	else if (stack->size == 2)
	{
		if (stack->arr[1].num > stack->arr[0].num)
		{
			operations(stacks, "sa");
			ft_putendl("sa");
		}
	}
}

static void				sort_3(t_stacks **stacks)
{
	t_stack				*stack;

	stack = (*stacks)->stack_a;
	if (stack->size == 3)
	{
		if (stack->arr[2].num > stack->arr[1].num && stack->arr[2].num > stack->arr[0].num)
		{
			operations(stacks, "ra");
			ft_putendl("ra");
		}
		else if (stack->arr[1].num > stack->arr[2].num && stack->arr[1].num > stack->arr[0].num)
		{
			operations(stacks, "rra");
			ft_putendl("rra");
		}
	}
	update_stacks(stacks);
	top_flip(stacks);
	while ((*stacks)->stack_b->size > 0)
	{
		operations(stacks, "pa");
		ft_putendl("pa");
	}
}

void					quick_sort(t_stacks **stacks)
{
	while ((*stacks)->stack_a->size > 3)
		mk_3(stacks);
	update_stacks(stacks);
	sort_3(stacks);
}