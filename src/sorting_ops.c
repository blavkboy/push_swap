/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_ops.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/22 15:30:39 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/08/22 15:30:41 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

static void		down_less(int up, int down, stack_int n, t_stacks **stacks)
{
	if (n.rot > 0 && n.rot < up && up - n.rot <= down)
		side_up(up, n, stacks);
	else if (n.rot > 0 && n.rot < up && up - n.rot >= down)
		mid_up_down(0, down, n, stacks);
	else if (n.rot < 0)
		side_down(down, n, stacks);
}

static void		up_less(int up, int down, stack_int n, t_stacks **stacks)
{
	if (n.rot < 0 && abs(n.rot) < down && down - n.rot <= up)
		side_down(down, n, stacks);
	else if (n.rot < 0 && abs(n.rot) < down && down - abs(n.rot) >= up)
		mid_up_down(up, down, n, stacks);
	else if (n.rot < 0 && abs(n.rot) < down && down - abs(n.rot) <= up)
		side_down(down, n, stacks);
	else if (n.rot > 0)
		side_up(up, n, stacks);
}

void			best_move(stack_int n, t_stacks **stacks)
{
	int			up;
	int			down;

	down = n.sort_down;
	up = n.sort_up;
	if (down == 0 && up == 0)
		rot_only(n, stacks);
	else if (n.rot == 0)
		short_sides(up, down, stacks);
	else if (down == up && n.rot > 0)
		side_up(up, n, stacks);
	else if (down == up && n.rot < 0)
		side_down(down, n, stacks);
	else if (n.rot > 0 && n.rot >= up)
		side_up(up, n, stacks);
	else if (n.rot < 0 && abs(n.rot) >= down)
		side_down(down, n, stacks);
	else if (down < up)
		down_less(up, down, n, stacks);
	else if (up < down)
		up_less(up, down, n, stacks);
}