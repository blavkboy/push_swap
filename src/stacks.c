/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stacks.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/11 14:27:50 by gladi8r           #+#    #+#             */
/*   Updated: 2018/08/12 14:00:22 by gladi8r          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

t_stacks			*init_stacks(void)
{
	t_stacks		*stacks;

	if ((stacks = ft_memalloc(sizeof(t_stacks))))
	{
		if (!(stacks->stack_a = ft_init_stack()))
			return (NULL);
		if (!(stacks->stack_b = ft_init_stack()))
			return (NULL);
		ft_bzero((void*)&stacks->args, 2 * sizeof(int));
		stacks->eval = 0;
		stacks->moves = 0;
	}
	return (stacks);
}

static long long	ft_atoll(char *num)
{
	long long		dig;
	int				sign;

	dig = 0;
	sign = (num[0] == '-'? -1: 1);
	if (num[0] == '-')
		num++;
	while (ft_isdigit(*num) && *num)
	{
		dig = dig * 10 + (*num - 48);
		num++;
	}
	return (dig * sign);
}

t_stacks			*make_stacks(t_stacks **stacks, char *arg)
{
	size_t			i;

	i = 0;
	while (arg[i])
	{
		if (arg[i] == '-' && ft_isdigit(arg[i + 1]))
		{
			ft_stack_push(&(*stacks)->stack_b, ft_atoi(&arg[i]));
			i++;
			while (ft_isdigit(arg[i]))
				i++;
		}
		else if (ft_isdigit(arg[i]))
		{
			if (ft_atoll(&arg[i]) > 2147483647 || ft_atoll(&arg[i]) < -2147483647)
				return (NULL);
			ft_stack_push(&(*stacks)->stack_b, ft_atoi(&arg[i]));
			while (ft_isdigit(arg[i]))
				i++;
		}
		else
			i++;
	}
	return (*stacks);
}

void				empty_stacks(t_stacks **stacks)
{
	if ((*stacks)->stack_a->arr)
		ft_memdel((void**)&(*stacks)->stack_a->arr);
	if ((*stacks)->stack_b->arr)
		ft_memdel((void**)&(*stacks)->stack_b->arr);
	ft_memdel((void**)&(*stacks)->stack_a);
	ft_memdel((void**)&(*stacks)->stack_b);
	ft_memdel((void**)stacks);
}

void				operations(t_stacks **stacks, char *line)
{
	if (!ft_strcmp(line, "ra"))
		rot_stack(stacks, "a");
	if (!ft_strcmp(line, "rb"))
		rot_stack(stacks, "b");
	if (!ft_strcmp(line, "rr"))
		rot_stack(stacks, "rr");
	if (!ft_strcmp(line, "rra"))
		rot_stack(stacks, "aa");
	if (!ft_strcmp(line, "rrb"))
		rot_stack(stacks, "bb");
	if (!ft_strcmp(line, "rrr"))
		rot_stack(stacks, "-r");
	if (!ft_strcmp(line, "pa"))
		push_stacks(stacks, "pa");
	if (!ft_strcmp(line, "pb"))
		push_stacks(stacks, "pb");
	if (!ft_strcmp(line, "sa"))
		swap_pop(stacks, "a");
	if (!ft_strcmp(line, "sb"))
		swap_pop(stacks, "b");
	if (!ft_strcmp(line, "ss"))
		swap_pop(stacks, "s");
	(*stacks)->moves += 1;
}