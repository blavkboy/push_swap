/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printing.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/22 16:08:11 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/08/22 16:08:20 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

static void			print_a(t_stacks **stacks, size_t i)
{
	while (i)
	{
		if ((*stacks)->eval)
		{
			ft_putstr("\e[0;32mRotations: ");
			ft_putnbr((*stacks)->stack_a->arr[i - 1].rot);
			ft_putstr("\e[0m ");
			ft_putstr("\e[0;96mSorting: ");
			ft_putnbr((*stacks)->stack_a->arr[i - 1].sort_up);
			ft_putstr("<- up & down ->");
			ft_putnbr((*stacks)->stack_a->arr[i - 1].sort_down);
			ft_putstr("\e[0m ");
			ft_putstr("score: ");
			ft_putnbr(get_score((*stacks)->stack_a->arr[i - 1]));
			ft_putchar(' ');
		}
		ft_putnbr((*stacks)->stack_a->arr[i - 1].num);
		ft_putstr("| ");
		i--;
	}
}

static void			print_b(t_stacks **stacks, size_t i)
{
	while (i)
	{
		if ((*stacks)->eval)
		{
			ft_putstr("\e[0;32mRotations: ");
			ft_putnbr((*stacks)->stack_b->arr[i - 1].rot);
			ft_putstr(" <-\e[0m ");
		}
		ft_putnbr((*stacks)->stack_b->arr[i - 1].num);
		ft_putstr("| ");
		i--;
	}
}

void				handle_args(t_stacks **stacks)
{
	size_t			i;

	update_stacks(stacks);
	if ((*stacks)->args[0])
	{
		i = (*stacks)->stack_a->size;
		ft_putscyan("Stack a = [ ");
		print_a(stacks, i);
		ft_putscyan("]\n");
		i = (*stacks)->stack_b->size;
		ft_putspurple("Stack b = [ ");
		print_b(stacks, i);
		ft_putspurple("]\n");
	}
	if ((*stacks)->args[1])
	{
		ft_putsblue("Number of moves: ");
		ft_putnbr((*stacks)->moves - ((*stacks)->stack_a->size +
			(*stacks)->stack_b->size));
		ft_putchar('\n');
	}
}