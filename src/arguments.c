/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arguments.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/11 14:24:56 by gladi8r           #+#    #+#             */
/*   Updated: 2018/08/13 10:46:14 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

int					passable_arg(char *arg)
{
	if (arg[0] == '-')
	{
		if (ft_isdigit(arg[1]))
			return (1);
		if (arg[1] == 'v')
			return (1);
		if (arg[1] == 'c')
			return (1);
	}
	if (ft_isdigit(arg[0]) || arg[0] == ' ')
		return (1);
	return (0);
}

int					check_args(char *arg)
{
	size_t			i;

	i = 0;
	while (arg[i])
	{
		if (passable_arg(&arg[i]))
		{
			i++;
			if ((arg[i] == 'c' || arg[i] == 'v') && (arg[i + 1] == ' ' || arg[i + 1] == '\0'))
				i++;
		}
		else
			return (0);
	}
	return (1);
}

int					*get_args(t_stacks **stacks, char **av)
{
	size_t			i;
	size_t			j;

	i = 0;
	while (av[i])
	{
		j = 0;
		while (av[i][j])
		{
			if (av[i][j] == 'v')
				(*stacks)->args[0] = 1;
			if (av[i][j] == 'c')
				(*stacks)->args[1] = 1;
			j++;
		}
		if ((*stacks)->args[0] && (*stacks)->args[1])
			break ;
		i++;
	}
	return ((*stacks)->args);
}