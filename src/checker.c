/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 13:23:35 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/08/11 15:44:58 by gladi8r          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

static void				reverse(t_stacks **stacks)
{
	while ((*stacks)->stack_b->size > 0)
		operations(stacks, "pa");
}

static int				parse_args(char **av)
{
	size_t			i;
	
	i = 1;
	while(av[i])
	{
		if (check_args(av[i]))
			i++;
		else
		{
			ft_putendl("Error");
			return (0);
		}
	}
	return (1);
}

static int				get_data(t_stacks **stacks, char **av)
{
	size_t				i;

	i = 1;
	while (av[i])
	{
		if (!make_stacks(stacks, av[i]))
		{
			ft_putendl("Error");
			return (0);
		}
		i++;
	}
	get_args(stacks, &av[1]);
	if (!confirm_set(stacks))
	{
		ft_putendl("Error");
		return (0);
	}
	return (1);
}

int					main(int ac, char **av)
{
	t_stacks		*stacks;
	char			*line;

	line = NULL;
	if (ac > 1)
	{
		stacks = init_stacks();
		if (!parse_args(av))
			return (1);
		if (!get_data(&stacks, av))
			return (1);
		reverse(&stacks);
		while (get_next_line(0, &line))
		{
			operations(&stacks, line);
			handle_args(&stacks);
			free(line);
		}
		confirm_sort(stacks);
		empty_stacks(&stacks);
		return (0);
	}
	ft_putendl("Error");
	return	(1);
}