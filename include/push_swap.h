/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/07 14:50:14 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/08/13 16:28:51 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

#include <libft.h>
#include <stack.h>

typedef struct		s_stacks
{
	t_stack			*stack_a;
	t_stack			*stack_b;
	int				args[2];
	int				eval;
	int				moves;
}					t_stacks;

void				confirm_sort(t_stacks *stacks);
void				num_rotations(t_stacks **stacks);
t_stacks			*init_stacks(void);
t_stacks			*make_stacks(t_stacks **stacks, char *arg);
void				empty_stacks(t_stacks **stacks);
void				rot_stack(t_stacks **stacks, char *str);
void				swap_a(t_stacks **stacks);
void				swap_b(t_stacks **stacks);
void				swap_pop(t_stacks **stacks, char *str);
void				push_stacks(t_stacks **stacks, char *str);
int					passable_arg(char *arg);
int					check_args(char *arg);
int					*get_args(t_stacks **stacks, char **av);
void				sort_stacks(t_stacks **stacks);
void				quick_sort(t_stacks **stacks);
void				handle_args(t_stacks **stacks);
void				operations(t_stacks **stacks, char *line);
void				update_stacks(t_stacks **stacks);
int					stack_top(t_stack *stack);
int					stack_bottom(t_stack *stack);
size_t				get_score(stack_int n);
void				best_move(stack_int n, t_stacks **stacks);
stack_int			find_biggest(t_stacks **stacks);
void				mid_up_down(int up, int down, stack_int n, t_stacks **stacks);
void				side_down(int down, stack_int n, t_stacks **stacks);
void				side_up(int up, stack_int n, t_stacks **stacks);
void				short_sides(int up, int down, t_stacks **stacks);
void				rot_only(stack_int n, t_stacks **stacks);
int					confirm_set(t_stacks **stacks);

#endif
