# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/08/11 15:06:30 by gladi8r           #+#    #+#              #
#    Updated: 2018/08/23 16:06:42 by gmohlamo         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

FILES = src/arguments.c src/operations.c src/sorted.c src/stacks.c \
	src/analysis.c src/rotations.c src/sorting_up.c src/printing.c \
	src/quick_sort.c src/sorting.c src/sorting_ops.c
OBJ = arguments.o operations.o sorted.o stacks.o analysis.o inquiries.o \
	rotations.o sorting_up.o printing.o quick_sort.o sorting_ops.o sorting.o
CHECK = src/checker.c
CHECKER = checker
PUSH_SWAP = push_swap
FLAGS = -Wall -Wextra -Werror
STACKS = -L./stacks -lstack
LIBFT = -L./libft -lft
HEAD = -I./include -I./libft -I./stacks
INC = include/push_swap.h

all: libs $(PUSH_SWAP) $(CHECKER)

$(CHECKER): $(OBJ) checker.o
	gcc $(HEAD) $(FLAGS) -o $(CHECKER) $(OBJ) checker.o $(STACKS) $(LIBFT)

$(PUSH_SWAP): $(OBJ) push_swap.o
	gcc $(HEAD) $(FLAGS) -o $(PUSH_SWAP) $(OBJ) $(OBJ1) push_swap.o $(STACKS) $(LIBFT)

OBJ: arguments.o operations.o sorted.o stacks.o analysis.o inquiries.o rotations.o \
	sorting_up.o printing.o quick_sort.o sorting_ops.o sorting.o

libs:
	make -C libft/
	make -C stacks/

arguments.o: src/arguments.c $(INC)
	gcc $(HEAD) $(FLAGS) -c src/arguments.c

operations.o: src/operations.c $(INC)
	gcc $(HEAD) $(FLAGS) -c src/operations.c

sorted.o: src/sorted.c $(INC)
	gcc $(HEAD) $(FLAGS) -c src/sorted.c

stacks.o: src/stacks.c $(INC)
	gcc $(HEAD) $(FLAGS) -c src/stacks.c

checker.o: src/checker.c $(INC)
	gcc $(HEAD) $(FLAGS) -c src/checker.c

push_swap.o: src/push_swap.c $(INC)
	gcc $(HEAD) $(FLAGS) -c src/push_swap.c

analysis.o: src/analysis.c $(INC)
	gcc $(HEAD) $(FLAGS) -c src/analysis.c

inquiries.o: src/inquiries.c $(INC)
	gcc $(HEAD) $(FLAGS) -c src/inquiries.c

rotations.o: src/analysis.c $(INC)
	gcc $(HEAD) $(FLAGS) -c src/rotations.c

quick_sort.o: src/quick_sort.c $(INC)
	gcc $(HEAD) $(HEAD) -c src/quick_sort.c

sorting_up.o: src/sorting_up.c $(INC)
	gcc $(HEAD) $(FLAGS) -c src/sorting_up.c

sorting_ops.o: src/sorting_ops.c $(INC)
	gcc $(HEAD) $(FLAGS) -c src/sorting_ops.c

sorting.o: src/sorting.c $(INC)
	gcc $(HEAD) $(FLAGS) -c src/sorting.c

printing.o: src/printing.c $(INC)
	gcc $(HEAD) $(FLAGS) -c src/printing.c

progs: libs all

re: fclean libs all

clean:
	rm -rf $(OBJ) checker.o push_swap.o
	make clean -C libft/
	make clean -C stacks/

fclean: clean
	rm -rf $(CHECKER) $(PUSH_SWAP)
	make fclean -C libft/
	make fclean -C stacks/
