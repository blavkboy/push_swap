/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_stack.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/27 12:27:37 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/08/12 14:02:23 by gladi8r          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

t_stack		*ft_init_stack(void)
{
	t_stack	*stack;

	stack = ft_memalloc(sizeof(t_stack));
	stack->arr = ft_memalloc(sizeof(stack_int) * 1000);
	stack->size = 0;
	return (stack);
}
