/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stack_pop.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/27 11:18:11 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/08/12 14:04:24 by gladi8r          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

stack_int		ft_stack_pop(t_stack **stack)
{
	stack_int	num;
	stack_int	zero;

	zero.num = 0;
	zero.rot = 0;
	zero.sort_up = 0;
	zero.sort_down = 0;
	if ((*stack)->size)
	{
		num = (*stack)->arr[(*stack)->size - 1];
		(*stack)->arr[(*stack)->size - 1] = zero;
		(*stack)->size -= 1;
		return (num);
	}
	return (zero);
}
