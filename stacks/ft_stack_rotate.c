/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stack_rotate.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/27 11:35:42 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/08/12 06:56:24 by gladi8r          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

t_stack			*ft_stack_rotate(t_stack **stack)
{
	stack_int	temp;

	if (!(*stack)->size)
		return (*stack);
	temp = (*stack)->arr[(*stack)->size - 1];
	ft_memmove((void*)&(*stack)->arr[1], (void*)(*stack)->arr,
			sizeof(stack_int) * ((*stack)->size - 1));
	(*stack)->arr[0] = temp;
	return (*stack);
}

t_stack			*ft_stack_rev_rotate(t_stack **stack)
{
	stack_int	temp;

	if (!(*stack)->size)
		return (*stack);
	temp = (*stack)->arr[0];
	ft_memmove((void*)(*stack)->arr, (void*)&(*stack)->arr[1],
			sizeof(stack_int) * ((*stack)->size - 1));
	(*stack)->arr[(*stack)->size - 1] = temp;
	return (*stack);
}
