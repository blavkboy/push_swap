/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stack_push.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/27 10:58:44 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/08/12 14:03:41 by gladi8r          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static stack_int	init_int(int content)
{
	stack_int	num;

	num.num = content;
	num.rot = 0;
	num.sort_up = 0;
	num.sort_down = 0;
	num.double_rot = 0;
	return (num);
}

t_stack			*ft_stack_push(t_stack **stack, int content)
{
	(*stack)->arr[(*stack)->size] = init_int(content);
	(*stack)->size += 1;
	return (*stack);
}
